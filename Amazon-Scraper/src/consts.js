exports.URLS = {
    DEFAULT: 'https://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=',
}

exports.LABELS = {
    SEARCHPAGE: 'search',
    PRODUCTPAGE: 'product',
    OFFERSPAGE: 'offers'
}

exports.STORAGE = {
    NAME: 'offers-num',
    WAIT_INTERVAL : 20000
}
