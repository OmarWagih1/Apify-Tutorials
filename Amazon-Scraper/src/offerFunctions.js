const Apify = require('apify');
const { STORAGE } = require('./consts');

const {
    utils: {
        log
    }
} = Apify;

const expandOfferList = 
    async function(page){
        const delay = 2500;
        const wait = (ms) => new Promise(res => setTimeout(res, ms));
        const count = async () => page.$$eval('#aod-offer-list hr', (childs) => childs.length);
        const scrollDown = async () => {
            await page.evaluate(() => {
                const element = document.querySelector('#all-offers-display-scroller');
                if ( element ) {
                    element.scrollTop = element.scrollHeight;
                }
             });
        }
    
        let preCount = 0;
        let postCount = 0;
        
        do {
        preCount = await count();
        await scrollDown();
        await wait(delay);
        postCount = await count();
        } while (postCount > preCount);

        do {
            preCount = await count();
            await page.click("#aod-show-more-offers").catch(() => {});
            await wait(delay);
            postCount = await count();
        } while (postCount > preCount);
    }

exports.offerFuncs = {
    getMainOffer : async function(page, productInfo){
        await page.waitForSelector('#all-offers-display');
            
        const mainPrice = await page.$$eval('#pinned-offer-top-id .a-price span.a-offscreen',
            ((price) => price[0].textContent.trim()));
        
        const mainSoldBy = await page.$$eval('#aod-pinned-offer #aod-offer-soldBy .a-col-right a, #aod-pinned-offer #aod-offer-soldBy .a-col-right span',
            ((soldBy) => soldBy[0].textContent.trim()));
        
        let mainDeliveryPrice = await page.$$eval('#pinned-offer-top-id span[data-csa-c-delivery-price]',
            ((deliveryPrice) => deliveryPrice[0].getAttribute('data-csa-c-delivery-price')));
        
        mainDeliveryPrice = mainDeliveryPrice.toLowerCase();
        if (mainDeliveryPrice == '') {
            mainDeliveryPrice = 'free';
        }
        
        log.info(`Main Offer found with price ${mainPrice} and a delivery price of ${mainDeliveryPrice} and sold by ${mainSoldBy}`)
        await Apify.pushData({
            ...productInfo,
            'sellerName': mainSoldBy,
            'price': mainPrice,
            'shippingPrice': mainDeliveryPrice,
        });

    }, 

    displayAllOffers : async function(page){
        try{
            await expandOfferList(page, '#aod-offer-list hr');
        }
        catch {
            log.info("Expanding caused an error");
        }
        
    },

    storeAllOffers : async function(page, productInfo, ASIN){
        const prices = await page.$$eval('#aod-offer #aod-offer-price .a-price .a-offscreen',
            ((prices) => prices.map((price) => price.textContent.trim() )));
            
        const deliveryPrices = await page.$$eval('#aod-offer #aod-offer-price .aod-delivery-promise',
            ((prices) => prices.map((price) => price.textContent.trim().split(' ')[0].toLowerCase() )));
        
        const soldBy = await page.$$eval('#aod-offer #aod-offer-soldBy .a-col-right',
            ((soldBys) => soldBys.map((soldBy) => soldBy.textContent.trim() )));
        
        log.info(`Found ${prices.length} offers`);

        const store = await Apify.openKeyValueStore();
        const previousState = await store.getValue(STORAGE.NAME,) || {};
        const newState = { ...previousState, [ASIN]: prices.length+1 };
        await store.setValue(STORAGE.NAME, newState);

        for(let i = 0; i < prices.length; i++){
            let deliverPrice = deliveryPrices[i];
            if(deliverPrice && deliverPrice.indexOf('$') == -1){
                deliverPrice = 'free';
            }
            await Apify.pushData({
                ...productInfo,
                'sellerName': soldBy[i],
                'price': prices[i],
                'shippingPrice': deliverPrice,
            });
        }
    }


}