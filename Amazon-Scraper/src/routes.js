const Apify = require('apify');
const { LABELS, URLS } = require('./consts');
const { utils: { log } } = Apify;
const { offerFuncs } = require('./offerFunctions');

exports.handleSearch =  async ({ page, crawler: { requestQueue }, request, session }) => {
    
    await page.waitForSelector('div[data-index]');
    let ASINs = await page.$$eval('div[data-asin]',
    (links) => links.map((link) => link.getAttribute('data-asin')).filter(ASIN => ASIN.length == 10))
    log.info('Number of ASINs Found:' + ASINs.length)
    log.info('List of ASINs Found: ' + ASINs)
    for (const ASIN of ASINs) {
            let url = `https://www.amazon.com/dp/${ASIN}`
            await requestQueue.addRequest({
                url: url,
                userData: {
                    keyword: request.userData.keyword,
                    label: LABELS.PRODUCTPAGE,
                    ASIN: ASIN,
                },
            });
        }

}


exports.handleProduct =  async ({ page, crawler: { requestQueue }, request, session }) => {
    
    await page.waitForSelector('#titleSection');
    let title = await page.$$eval('#productTitle', title => title[0].textContent.trim());
    let description = await page.$$eval('#feature-bullets li',
        ((bullets) => bullets.map((bullet) => bullet.textContent.trim())));
    let pageUrl = await page.url();
    let ASIN = request.userData.ASIN;
    let url = `https://www.amazon.com/gp/offer-listing/${ASIN}`
    log.info(`Product Title: ${title} with ASIN: ${ASIN}`)
    await requestQueue.addRequest({
        url: url,
        userData: {
            keyword: request.userData.keyword,
            label: LABELS.OFFERSPAGE,
            ASIN: ASIN,
            pageUrl: pageUrl,
            title:title,
            description: description,
        },
    });

};

exports.handleOffers =  async ({ page, crawler: { requestQueue }, request, session }) => {
    
    await page.waitForSelector('#titleSection');
    const ASIN = request.url.split('/').at(-1);
    let productInfo = 
    {   'title':request.userData.title,
        'description':request.userData.description,
        'url': request.userData.pageUrl,
        'keyword': request.userData.keyword
    }
    log.info(`Getting Main Offer for page ${page.url()}`);
    await offerFuncs.getMainOffer(page, productInfo); 

    log.info("Scrolling to display all offers");
    await offerFuncs.displayAllOffers(page);

    log.info("Storing All Offers");
    await offerFuncs.storeAllOffers(page, productInfo, ASIN);

};
