const Apify = require('apify')
const { LABELS, URLS, STORAGE } = require('./src/consts')
const { handleOffers, handleSearch, handleProduct } = require('./src/routes')

const {
    utils: { log }
} = Apify
Apify.main(async () => {
    const { keyword } = await Apify.getInput()
    let stats;

    log.info('Current Keyword is: ' + keyword)

    const requestList = await Apify.openRequestList('start-url', [
        {
            url: `${URLS.DEFAULT}${keyword}`,
            userData: {
                label: LABELS.SEARCHPAGE,
                keyword: keyword
            }
        }
    ])

    const requestQueue = await Apify.openRequestQueue();
    const proxyConfiguration = await Apify.createProxyConfiguration({
        groups: ['BUYPROXIES94952'],
    });


    const crawler = new Apify.PuppeteerCrawler({
        requestList,
        requestQueue,
        // I noticed with this proxy the scraper fails more often, why?
        proxyConfiguration,
        useSessionPool: true,
        persistCookiesPerSession: true,
        sessionPoolOptions: {
            maxPoolSize: 100,
            sessionOptions: {
                maxUsageCount: 5
            }
        },
        launchContext: {
            stealth: true
        },
        navigationTimeoutSecs: 120,
        handlePageFunction: async contextObject => {
            try {
                const title = await contextObject.page.title()
                if (
                    title === 'Amazon.com' ||
                    title === 'Sorry! Something went wrong!'
                ) {
                    log.error('Page is blocked. Session will be retired!');
                    contextObject.session.retire();
                    throw 'err';
                }
            } catch (err) {
                throw err;
            }

            switch (contextObject.request.userData.label) {
                case LABELS.SEARCHPAGE:
                    return handleSearch(contextObject)
                case LABELS.PRODUCTPAGE:
                    return handleProduct(contextObject)
                case LABELS.OFFERSPAGE:
                    return handleOffers(contextObject)
            }
        }
    })

    log.info('Crawling started')

    // This is not guaranteed to persist the storage 100% as if its a huge object it could take more than a few seconds to write to the file.
    Apify.events.on('migrating', () => {
        Apify.setValue(STORAGE.NAME, stats)
    });

    const showStats = setInterval(async () => {
        stats = await Apify.getValue(STORAGE.NAME)
        log.info("stats object: ", stats);
        if (await requestQueue.isFinished()) {
            clearInterval(showStats);
        }
    }, STORAGE.WAIT_INTERVAL);


    await crawler.run()

    const dataUrl = `https://api.apify.com/v2/datasets/${process.env.APIFY_DEFAULT_DATASET_ID}/items`

    // Sending email
    // await Apify.call('apify/send-mail', {
    //     to: 'lukas@apify.com',
    //     subject: 'Omar Wagih. This is for the Apify SDK exercise',
    //     text: `This is the data result for keyword: ${keyword}. Here's a link to the dataset: ${dataUrl}`,
    // });
})
