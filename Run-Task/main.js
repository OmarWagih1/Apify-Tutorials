const Apify = require('apify')
const { ApifyClient } = require('apify-client')
const axios = require('axios')
const {
    utils: { log }
} = Apify

const TOKEN = process.env.APIFY_TOKEN
const DELAY = 3000

const wait = ms => new Promise(res => setTimeout(res, ms))

Apify.main(async () => {
    let data
    const {
        memory,
        useClient,
        fields,
        maxItems,
        taskId,
        userId,
        actorId
    } = await Apify.getInput()

    // Get the data using Apify client
    if (useClient) {
        const apifyClient = new ApifyClient({
            token: TOKEN
        })
        const taskRun = await apifyClient.task(taskId).start({
            memory: memory
        })

        let isRunning = await apifyClient
            .task(taskId)
            .runs()
            .list({ desc: true })
        isRunning = isRunning.items[0]

        await (async () => {
            while (isRunning.status == 'RUNNING') {
                await wait(DELAY)
                log.info('Task is still running')
                isRunning = await apifyClient
                    .task(taskId)
                    .runs()
                    .list({ desc: true })
                isRunning = isRunning.items[0]
            }
        })()

        data = await apifyClient
            .dataset(taskRun.defaultDatasetId)
            .downloadItems('csv', {
                fields: fields
            })
    } else {
        const response = await axios.post(
            `https://api.apify.com/v2/actor-tasks/${taskId}/runs?token=${TOKEN}&memory=${memory}`
        )
        const defaultDatasetId = response.data.data.defaultDatasetId
        let isRunning = await axios.get(
            `https://api.apify.com/v2/acts/${actorId}/runs/last/?actorTaskId=${taskId}&token=${TOKEN}`
        )

        await (async () => {
            while (isRunning.data.data.status == 'RUNNING') {
                await wait(DELAY)
                log.info('Task is still running')
                isRunning = await axios.get(
                    `https://api.apify.com/v2/acts/${actorId}/runs/last/?actorTaskId=${taskId}&token=${TOKEN}`
                )
            }
        })()

        const field = fields.join()
        const datasetResponse = await axios.get(
            `https://api.apify.com/v2/datasets/${defaultDatasetId}/items?token=${TOKEN}&format=csv&limit=${maxItems}&fields=${field}`
        )
        data = datasetResponse.data
    }
    // Get the data using API

    await Apify.setValue('OUTPUT', data, {
        contentType: 'text/csv'
    })
})
