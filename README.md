# Apify SDK Tutorials

This is an excercise project on the Apify SDK.


## Tutorial II

1. Where and how can you use JQuery with the SDK?

   You can use JQuery with PuppeteerCrawler as it works by simulating a browser.

2. What is the main difference between Cheerio and JQuery?

   Cheerio implements a subset of the core of JQuery to work on the server side, not the full JQuery API. JQuery manipulates the DOM while 
   Cheerio parses html from an http response.

3. When would you use CheerioCrawler and what are its limitations?

   The Cheerio crawler is perfect for static pages that do not have javascript based content updates.

4. What are the main classes for managing requests and when and why would you use one instead of another?

   The main two classes are RequestList and RequestQueue, RequestList is used for when there is a defined set of URLs as it is static and immutable and thus much faster than RequestQueue while RequestQueue is a dynamic list that can be added to from inside the code.

5. How can you extract data from a page in Puppeteer without using JQuery?

   You can use standard methods like document.querySelectorAll().

6. What is the default concurrency/parallelism the SDK uses?

   By default, the max amount of memory to be used is set to one quarter of total system memory, i. e. on a system with 8192 MB of memory, the autoscaling feature will only use up to 2048 MB of memory.

## Tutorial III 

1. How do you allocate more CPU for your actor run?

   You allocate more memory to the actor and thus more CPU will be allocated.

2. How can you get the exact time when the actor was started from within the running actor process?

   By Accessimg the APIFY_STARTED_AT environment variable.

3. Which are the default storages an actor run is allocated (connected to)?

   The default storages are Datasets, key-value storage and RequestQueue.

4. Can you change the memory allocated to a running actor?
   
   No you can not, to change the memory allocated you need to restart the actor.

5. How can you run an actor with Puppeteer in a headful (non-headless) mode?
   
   You can add the option headless:false in Puppeteer's launch context.

6. Imagine the server/instance the container is running on has a 32 GB, 8-core CPU. What would be the most performant (speed/cost) memory allocation for CheerioCrawler? (Hint: NodeJS processes cannot use user-created threads)

   CheerioCrawler can only use a single CPU core as it is a NodeJS process so the allocation should be 4GB of ram and 1 CPU core.

(Bonus - Docker)

7. What is the difference between RUN and CMD Dockerfile commands?

   RUN command triggers while we build the docker image and CMD - command triggers while we launch the created docker image.

8. Does your Dockerfile need to contain a CMD command (assuming we don't want to use ENTRYPOINT which is similar)? If yes or no, why?

   Yes as we need to specify at least one of them to run the docker file.

9. How does the FROM command work and which base images Apify provides?

   FROM instruction specifies the underlying OS architecture that you are gonna use to build the image. The base images Apify provides are: 
   
    apify/actor-node
    apify/actor-node-puppeteer-chrome
    apify/actor-node-playwright
    apify/actor-node-playwright-chrome
    apify/actor-node-playwright-firefox
    apify/actor-node-playwright-webkit

## Tutorial IV

1. Do you have to rebuild an actor each time the source code is changed?

   Yes you do, as the code changes you want the build the actor is running to change.


2. What is the difference between pushing your code changes and creating a pull request?

   Pull requests merges different branches and displays the difference, if exists, between them. Push requests changes the source repository
   to match our own.

3. How does the apify push command work? Is it worth using, in your opinion?

   This command uploads your project to the Apify cloud and builds an actor from it, it creates a new actor so I think its only worth it
   if you are developing locally and would like to test your project on the cloud platform.

## Tutorial V

1. What is the relationship between actor and task?

   A task is basically a configuration to run a certain actor with.

2. What are the differences between default (unnamed) and named storage? Which one would you choose for everyday usage?

   The default storages for an actor run are created without a name (with only an ID). This allows them to expire after 7 days (on the free plan, longer on paid plans) and not take up storage space. If you want to preserve a storage, simply give it a name and it will be retained indefinitely.  I'd choose default storage for everyday usage as I'd probably only use the data I need within 7 days.

3. What is the relationship between the Apify API and the Apify client? Are there any significant differences?

   The Apify client is like a wrapper to the Apify API for different languages.

4. Is it possible to use a request queue for deduplication of product IDs? If yes, how would you do that?

   I'm not quite sure what this means, If some ID fails and we'd like to scrape it again a request queue will automatically add it.

5. What is data retention and how does it work for all types of storage (default and named)?

   Apify has a data storage that retains data for 7 days (free plan, unnamed) and indefinitely for named storages.

6. How do you pass input when running an actor or task via the API?

   Using a POST request to the API, you can pass as parameters in the URL.

## Tutorial VI

1. What types of proxies does the Apify Proxy include? What are the main differences between them?

   Shared Datacenter IPs - Cost effective, shared with other users, chance that IP is blocked due to other user activity.

   Dedicated datacenter IPs - Costs more, full control of the traffic which makes blocking and crawling rate predictable

   Residential IPs - These IPs have the lowest chance of blocking and best geographical coverage. Usage is charged based on data transferred.

   Google SERPs - Download Google Search engine or Google Shopping result pages using our specialized service. Usage is charged from your platform usage credits based on the number of requests submitted. 

2. Which proxies (proxy groups) can users access with the Apify Proxy trial? How long does this trial last?

   Google SERPs up to 100 requests and up to 20 shared datacenter IPs.

3. How can you prevent a problem that one of the hardcoded proxy groups that a user is using stops working (a problem with a provider)? What should be the best practices?

   SessionPool can be used to filtour out blocked or non-working proxies so the actor does not retry requests for them.

4. Does it make sense to rotate proxies when you are logged in?

   I don't think it makes sense, when you are logged in to a website it naturally expects you to do every action from a single IP.

5. Construct a proxy URL that will select proxies only from the US (without specific groups).

   http://,country-US:@proxy.apify.com:8000

6. What do you need to do to rotate proxies (one proxy usually has one IP)? How does this differ for Cheerio Scraper and Puppeteer Scraper?

   We can use SessionPool to rotate the proxies, for Cheerio this will rotate the proxy on every request while Puppeteer will only change the proxy when the browser is retired, it restarts the browser by default after every 100 requests but depending on the aggressiveness of the target website you can adjust this using retireInstanceAfterRequestCount.

7. Try to set up the Apify Proxy (using any group or auto) in your browser. This is useful for testing how websites behave with proxies from specific countries (although most are from the US). You can try Switchy Omega extension but there are many more. Were you successful?

   Yes it did, although surprisingly even though the username was country-US the country listed on Google was russia.

8. Name a few different ways a website can prevent you from scraping it.

   Blocking IP, if it produces an unnatural amount of requests.
   Making the user enter a captcha.
   Blocking access from cloud hosting and scraping service IP addresses.
   Rendering text in images.

9.  Do you know any software companies that develop anti-scraping solutions? Have you ever encountered them on a website?

   Not currently.

## Tutorial VII

1. Actors have a Restart on error option in their Settings. Would you use this for your regular actors? Why? When would you use it, and when not?

   I wouldn't use it on regular actors as websites often block scrapers and that can throw errors, I would maybe use it on static websites that don't have any anti-scraping measures as the error is then probably significant.

2. Migrations happen randomly, but by setting Restart on error and then throwing an error in the main process, you can force a similar situation. 
   Observe what happens. What changes and what stays the same in a restarted actor run?

   All processes will be restarted and while some data will be handled by the Apify SDK, some data will be lost if not persisted into a storage by a migration event.

3. Why don't you usually need to add any special code to handle migrations in normal crawling/scraping? Is there a component that essentially solves this problem for you?

   Because the Apify SDK persists its state automatically, using the migrating and persistState events. persistState notifies SDK components to persist their state at regular intervals in case a migration happens. The migrating event is emitted just before a migration.

4. How can you intercept the migration event? How much time do you need after this takes place and before the actor migrates?

   We can listening to the migration event. When a migration event occurs, you only have a few seconds to save your work.

5. When would you persist data to a default key-value store and when would you use a named key-value store?

   If the data is needed for more than 7 days we should use a named key-value store as a default key-value store will only be persisted for 7 days.