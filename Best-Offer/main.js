// This is the main Node.js source code file of your actor.

// Import Apify SDK. For more information, see https://sdk.apify.com/
const Apify = require('apify')

Apify.main(async () => {

   const apiResponse = await Apify.getInput();
    const importedDataset = await Apify.openDataset(apiResponse.resource.defaultDatasetId);
    const offers = (await importedDataset.getData()).items;
    
    let bestOffer = {}
    
    for (const product of offers) {
        let price = parseFloat(product.price.replace('$', ''))
        if (bestOffer[product.url] === undefined) {
            bestOffer[product.url] = product
        } else if (
            parseFloat(bestOffer[product.url].price.replace('$', '')) > price
        ) {
            bestOffer[product.url] = product
        }
    }
    var bestOfferArray = []
    for (var key in bestOffer) {
        await Apify.pushData(bestOffer[key])
    }
    
})
